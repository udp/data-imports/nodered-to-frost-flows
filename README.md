# NodeRed to FROST Flows

Die Flows innerhalb der JSON Importdatei enthalten einen Flow zur Speicherung von Daten in die Strukturen des FROST Servers. Für die Verwendung kann der Flow importiert werden. Die einzelnen Subflows können dann direkt in eigenen Flows genutzt werden. Einmalig muss im FROST Call Flow die Verbindung zum FROST Server angelegt werden. Hierbei ist darauf zu achten, dass die Logindaten sowie die Proxyeinstellungen korrekt gesetzt werden.
